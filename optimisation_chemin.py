from TSP_biblio import *

def table_distance(tour):
    table = []
    for i in range(len(tour)):
        table.append([])
        for j in range(len(tour)):
            table[i].append([])
            table[i][j].append(distance(tour,i,j))
    return table

def indice_proche(ville, liste_ville, table):
    distance_min = table[ville][liste_ville[0]]
    indice_min = liste_ville[0]    
    for i in range(len(liste_ville)):
        if table[ville][liste_ville[i]] < distance_min:
            distance_min = table[ville][liste_ville[i]]
            indice_min = liste_ville[i]
    return indice_min

            
def faire_tour(ville,liste_ville,table):
    liste_ville_1 = liste_ville
    tour = []
    tour.append(ville)
    while len(liste_ville_1) != 0:
        indice_p = indice_proche(ville,liste_ville_1,table)
        tour.append(indice_p)
        for i in range(len(liste_ville_1)):
            if liste_ville_1[i] == indice_p:
                k = i
        del liste_ville_1[k]  
    return tour
        
def afficher_tour(ville,liste_ville,table):
    tour = faire_tour(ville,liste_ville,table)
    destination = []
    tour_init = get_tour_fichier('exemple.txt')
    for i in range(len(tour)):
        destination.append(tour_init[tour[i]])
    trace(destination)
    
        
        
        
    

